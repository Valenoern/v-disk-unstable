#!/bin/bash
# general administration script for v-disks/archives

# assume final argument is command
arg=${!#}

# current directory
here=`pwd`
# determine whether this is "probably" a disk folder
inside_disk="n"
if [[ -d "contents" && -f "Makefile" ]]; then
  inside_disk="y"
fi

error_not_disk () {
 echo "This does not appear to be a disk folder."
}

changes () {
  # get the two most recent directory hashes, previous then recent
  hash_files=`ls -1 dir dir.* | head -2 | tac | tr "\n" " "`
  # print differences between the two directory hash lists
  diff $hash_files
}

if [[ "$arg" == "changes" ]]; then
  if [[ "$inside_disk" == "y" ]]; then
    changes
    exit 0
   else
    error_not_disk
    exit 1
  fi
 else
  echo "No operation specified" 1>&2
  exit 2
fi
