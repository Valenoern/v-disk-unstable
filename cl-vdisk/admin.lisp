;;;; admin.lisp - administer saved files for v-disks {{{
;; at least for now, this will serve as the "new package solely for LEAF nonsense"
(defpackage :vxsh-suite.disk.admin
	(:shadowing-import-from :vxsh-suite.poa
		#:leaf  #:leaf-p #:make-leaf
		#:leaf-path #:leaf-hash #:leaf-id #:leaf-stem #:leaf-meta #:leaf-body)
	(:use :common-lisp)
	(:local-nicknames
		(:poa :vxsh-suite.poa)
		)
	(:export
		#:*vhash-data*
		
		#:make-disk  #:disk-path #:disk-hash #:disk-instance #:disk-title  #:disk-name<
		#:disk-fullname #:disk-basename
))
(in-package :vxsh-suite.disk.admin)

(defvar *vhash-data*)
	;; by default, this is ~/.local/share/vhash/
	;; in the original shell script it was /var/lib/vhash/, but to be honest, I do not actually know how to ask for privileges inside a Lisp script. so this is the new default

#|
.
└── EXAMPLE_r1
    ├── EXAMPLE
    └── EXAMPLE-B
|#

;; }}}


;;; DISK structure

(defun make-disk (&key path instance title)  ; create DISK/LEAF structure for disk folders
	(make-leaf
		:path path  :id instance  :stem title))

(defun disk-path     (disk)  (leaf-path disk))
(defun disk-hash     (disk)  (leaf-hash disk))
(defun disk-instance (disk)  (leaf-id   disk))  ; :instance -> :id
(defun disk-title    (disk)  (leaf-stem disk))  ; :title    -> :stem

(defun (setf disk-path) (value disk)  (setf (leaf-path disk) value))

(defun disk-name< (x y) ; whether DISK/LEAF should sort before by :title
	(string<
		(disk-instance x)
		(disk-instance y)))

;;; DISK path functions

(defun disk-fullname (disk-path)  ; disk folder name as string, ex. SAMPLE.001
	(let (fullname)
	(when (pathnamep disk-path)
		(setq fullname
			(car (last (pathname-directory disk-path)))
			))
	
	;; remove "." from hidden files
	(when (and (stringp fullname) (equal (elt fullname 0) #\.))
		(setq fullname (subseq fullname 1 (length fullname))))
	
	fullname))

(defun disk-basename (disk-path)  ; title of disk, ex. SAMPLE.001 -> SAMPLE
	(let* ((fullname (disk-fullname disk-path))
	       (basename fullname)
	       (basename-end (- (length basename) 1))
	       period)
	
	(loop for i  from basename-end  downto 1  until (not (null period))  do
		(when (equal (elt basename i) #\.)  (setq period i)))
	
	(unless (null period)
		(setq basename (subseq basename 0 period)))
	
	basename))



