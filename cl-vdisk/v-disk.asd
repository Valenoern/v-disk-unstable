(defsystem "v-disk"
	:author "Valenoern"
	:licence "GPL"  ; GPL 3 or later
	:description "create self-contained \"archival units\" and monitor them for changes [not to be confused with virtual hard drives]"
	:homepage "https://codeberg.org/vxsh-suite/v-disk"
	
	:depends-on (
		"uiop"
		"file-attributes" ; modified time - ZLIB
		"ironclad"        ; hash/digest - MIT
		"bop-recode"      ; entry IDs / simple string parsing - GPL3
		)
	:components (
		(:file "admin")   ; :vxsh-suite.disk.admin
		(:file "hash")    ; :vxsh-suite.hash
		(:file "dredge")  ; :vxsh-suite.disk.dredge
		;; (:file "command")  ; :vxsh-suite.disk
))


;; possible redundant folders per disc — cr. 1691286938
;; 232  - 3   174  - 4   140  - 5   116  - 6   100  - 7
