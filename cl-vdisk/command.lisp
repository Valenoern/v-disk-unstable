;;;; command.lisp - general administration script for v-disks/archives
(defpackage :vxsh-suite.disk
	(:use :common-lisp)
	(:export
		;; #::error-not-disk
		#:inside-disk-p
		#:disk-changes
))
(in-package :vxsh-suite.disk)


(defun error-not-disk () ; {{{
	"This does not appear to be a disk folder."
) ; }}}

(defun inside-disk-p () ; {{{
;; determine whether this is "probably" a disk folder
	(if
		;; as of v-disk 1.8.0, a disk folder typically always contains "contents/" and "Makefile"
		(and
			(uiop:directory-exists-p "contents")
			(uiop:file-exists-p "Makefile"))
		t
		nil)
) ; }}}

(defun disk-changes ()
	;; get the two most recent directory hashes, previous then recent
		;; hash_files=`ls -1 dir dir.* | head -2 | tac | tr "\n" " "`
	;; print differences between the two directory hash lists
		;; diff $hash_files
	(if (inside-disk-p)
		(print "disk changes")   ; should "exit 0"
		(print (error-not-disk)) ; should "exit 1"
		)
)

;; LATER: invoking this through a "$ vdisk changes" type of interface can only really come when vxsh is more developed
