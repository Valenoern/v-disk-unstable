;;; demo to redundantly replicate information for a physical v-disk
;; as we have a need to get something working quickly, for now I am generating k3b projects
;; one day maybe v-refracta will just have its own CD burning program

(princ "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<!DOCTYPE k3b_data_project>
<k3b_data_project>
<general>
<writing_mode>auto</writing_mode>
<dummy activated=\"no\"/>
<on_the_fly activated=\"yes\"/>
<only_create_images activated=\"no\"/>
<remove_images activated=\"yes\"/>
</general>
<options>
<rock_ridge activated=\"yes\"/>
<joliet activated=\"yes\"/>
<udf activated=\"no\"/>
<joliet_allow_103_characters activated=\"yes\"/>
<iso_allow_lowercase activated=\"no\"/>
<iso_allow_period_at_begin activated=\"no\"/>
<iso_allow_31_char activated=\"yes\"/>
<iso_omit_version_numbers activated=\"no\"/>
<iso_omit_trailing_period activated=\"no\"/>
<iso_max_filename_length activated=\"no\"/>
<iso_relaxed_filenames activated=\"no\"/>
<iso_no_iso_translate activated=\"no\"/>
<iso_allow_multidot activated=\"no\"/>
<iso_untranslated_filenames activated=\"no\"/>
<follow_symbolic_links activated=\"no\"/>
<create_trans_tbl activated=\"no\"/>
<hide_trans_tbl activated=\"no\"/>
<iso_level>3</iso_level>
<discard_symlinks activated=\"no\"/>
<discard_broken_symlinks activated=\"no\"/>
<preserve_file_permissions activated=\"no\"/>
<do_not_cache_inodes activated=\"yes\"/>
<whitespace_treatment>noChange</whitespace_treatment>
<whitespace_replace_string>_</whitespace_replace_string>
<data_track_mode>auto</data_track_mode>
<multisession>auto</multisession>
<verify_data activated=\"no\"/>
</options>
<header>
<volume_id>COURAGE.r1</volume_id>
<volume_set_id>COURAGE</volume_set_id>
<volume_set_size>1</volume_set_size>
<volume_set_number>1</volume_set_number>
<system_id>LINUX</system_id>
<application_id>K3B THE CD KREATOR (C) 1998-2018 SEBASTIAN TRUEG, MICHAL MALEK AND LESLIE ZHAI</application_id>
<publisher></publisher>
<preparer></preparer>
</header>
<files>
")

(loop for i from 1 to 3 do
    (format t
"<directory name=\"~aTYRANNOMON.~3,'0d\">
<file name=\"Brave Heart Digimon 01 01 Cover By Bankiiz.mp3\">
<url>/home/takumi/DIGIMON_COPY/Brave Heart Digimon 01 01 Cover By Bankiiz.mp3</url>
</file>
<file name=\"Animelmack - Brave Heart Digimon 01dVpptXs9k.mp3\">
<url>/home/takumi/DIGIMON_COPY/Animelmack - Brave Heart Digimon 01dVpptXs9k.mp3</url>
</file>
<file name=\"Digimon Wir werden Siegen Long version.mp3\">
<url>/home/takumi/DIGIMON_COPY/Digimon Wir werden Siegen Long version.mp3</url>
</file>
<file name=\"Wada Kouji Seven.mp3\">
<url>/home/takumi/DIGIMON_COPY/Wada Kouji Seven.mp3</url>
</file>
<file name=\"Not So Great channel - Xtreme Fight BEST QUALITY.mp3\">
<url>/home/takumi/DIGIMON_COPY/Not So Great channel - Xtreme Fight BEST QUALITY.mp3</url>
</file>
<file name=\"Digimon Adventure Theme Brave Heart JackieO Russian Version.mp3\">
<url>/home/takumi/DIGIMON_COPY/Digimon Adventure Theme Brave Heart JackieO Russian Version.mp3</url>
</file>
<file name=\"Brave Heart Digimon Adventure Tri. Version (rec 100 175).mp3\">
<url>/home/takumi/DIGIMON_COPY/Brave Heart Digimon Adventure Tri. Version (rec 100 175).mp3</url>
</file>
<file name=\"brave heart Covered by ANIMAL MANIA.mp3\">
<url>/home/takumi/DIGIMON_COPY/brave heart Covered by ANIMAL MANIA.mp3</url>
</file>
</directory>
"
        (if (> i 1) "." "")
        i
))

(loop for i from 1 to 9000 do
    (format t
"<directory name=\"~aKEY.~4,'0d\">
<directory name=\"NOTE_OR_FILE_DIRS_ARE_EMPTY\"/>
<directory name=\"FILE_reversedragon3_internet-archive\"/>
<directory name=\"FILE_bergfalkr0_sovietize-github-workersunite\"/>
<directory name=\"FILE_valenoern_distributary\"/>
<directory name=\"FILE_0-list-txt\"/>
<file name=\"digest\">
<url>/tmp/key/digest</url>
</file>
<file name=\"dir\">
<url>/tmp/key/dir</url>
</file>
<file name=\"reversedragon3_internet-archive\">
<url>/tmp/key/reversedragon3_internet-archive</url>
</file>
<file name=\"bergfalkr0_sovietize-github-workersunite\">
<url>/tmp/key/bergfalkr0_sovietize-github-workersunite</url>
</file>
<file name=\"valenoern_distributary\">
<url>/tmp/key/valenoern_distributary</url>
</file>
<file name=\"0-list.txt\">
<url>/tmp/key/0-list.txt</url>
</file>
</directory>
"
        (if (> i 1) "." "")
        i
))


(princ "</files>
</k3b_data_project>")
