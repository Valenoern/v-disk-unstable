#!/bin/bash
# attempt to find ID of bopwiki entry
# may be copied/moved to minibop later

# usage: bop_id.sh FILENAME
file=$1

# sed regex required to match a standard ::/time line
# BUG: this is not perfect, for one it botches "::" lines with multiple values. but it took a lot of effort to even get it to work this well, so I'm not fixing it right now
_icon='\([\#\;]\s*\)\?::'
_regex=`echo -n "s/^${_icon}\s\([^0-9]\+\s\)\?\(.\+\)/\3/"`

# if this is "probably" a bop entry or text file
_text_file=`file -i $file | grep "text"`
if [[ -n "$_text_file" ]]; then
 # get the first "::" line,
 # and select the last part of the line up to the last space character
 grep "::\s" $file -m 1 | sed "$_regex"
fi
